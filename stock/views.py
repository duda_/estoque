from django.views.generic import ListView, DetailView
from django.shortcuts import redirect
from stock.models import Product
from django.db.models import Q

class ProductListView(ListView):
    model = Product

class OutOfStockListView(ListView):
    model = Product

    def get_queryset(self):
        products = super().get_queryset()
        return products.filter(quantidade=0)

class AddStockView(DetailView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade += int(quantidade)
        product.save()
        return redirect('product_list')

class RemoveStockView(DetailView):
    model = Product
    
    def post(self, request, *args, **kwargs):
        product = self.get_object()
        quantidade = self.request.POST['quantidade']
        product.quantidade -= int(quantidade)
        product.save()
        return redirect('product_list')

class SearchView(ListView):
    model = Product
    template_name = 'stock/search.html'

    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Product.objects.filter (
            Q(nome__icontains=query) |
            Q(descricao__icontains=query) 
        )
        return object_list
