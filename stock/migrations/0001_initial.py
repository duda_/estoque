# Generated by Django 2.2.3 on 2019-08-05 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=45)),
                ('descricao', models.TextField()),
                ('codigo', models.CharField(max_length=20)),
                ('imagem', models.ImageField(upload_to='produtos')),
                ('quantidade', models.PositiveIntegerField()),
            ],
        ),
    ]
